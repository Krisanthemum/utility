﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Utility
{
    /// <summary>Stores </summary>
    public class Settings : Dictionary<string, string>
    {
        string SettingsPath = Path.ChangeExtension(AppContext.BaseDirectory, "*.xml");
        public Settings()
        {
            
            string[] sets = File.ReadAllLines(SettingsPath);
            foreach(string s in sets)
            {
                string[] temp = s.Split('=');
                Add(temp[0], temp[1]);
            }
        }

        public void Save()
        {
            List<string> lines = new List<string>();
            foreach(string key in Keys)
            {
                lines.Add(key + "=" + this[key]);
            }

            File.WriteAllLines(SettingsPath, lines);
        }

        public T GetByType<T>(string key)
        {
            try
            {
                return (T)Convert.ChangeType(this[key], typeof(T));
            }
            catch { }
            return default(T);
        }
    }
}
